export const mySort = (items,name) => {
   return items.sort(function (a, b) {
        if (a[name] > b[name]) {
            return 1;
        }
        if (a[name] < b[name]) {
            return -1;
        }
        return 0;
    });
}
export const addListAndSort = (list1,list2,name) => {
  return mySort(list1.concat(list2),name);
}
export const baseUrl = () => {
    return "http://192.168.43.241:1122/api/soccer";
}