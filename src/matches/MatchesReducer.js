import { createSlice } from "@reduxjs/toolkit";
import { setApi } from "../ApiCall";
const slice = createSlice({
    name: "matches",
    initialState: {
        result: {},
        matches: [],
        matchStatistics: {},
        liveMatches: [],
        error: {},

    },
    reducers: {
        matchesReducer: (state, action) => {
            state.matches = action.payload;
        },
        matchesStatistics: (state, action) => {
            state.matchStatistics = action.payload;
        },
        liveMatchesReducer: (state, action) => {
            state.liveMatches = action.payload;
        },
        result: (state, action) => {
            state.result = action.payload;
        },
        error: (state, action) => {
            state.error = action.payload;
        },

    }
})

function getToken() {
    return localStorage.getItem("Authorization");
}
function getRole() {
    return localStorage.getItem("role");
}

function getId() {
    return localStorage.getItem("id");
}

export const getMatchesStatistic = (id) => setApi({
    url: "/match/"+id,
    method: "get",
    success: slice.actions.matchesStatistics,
    error: slice.actions.error
});

export const getMatches = (data) => setApi({
    url: "/api/user/getStats/" + data,
    method: "get",
    success: slice.actions.matchesReducer,
    error: slice.actions.error
});

export default slice.reducer;