import React, {useState} from 'react';
import {useParams} from "react-router-dom";
import {useEffect, useRef} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getMatchesStatistic} from "./MatchesReducer";
import {Col, Container, Row, Tab, Tabs} from "react-bootstrap";
import homeStyle from './home.module.scss'
import ball from '../image/img_4.png'
import yellow from '../image/yellowCard.png'
import red from '../image/redCard.png'
import {RiArrowUpDownFill} from "react-icons/ri";

export const checkStatusMatch = (name, helper) => {
    if (name === "Goal") {
        return <img src={ball} alt="" width={20}/>
    } else if (name === "Card") {
        if (helper === "Yellow Card") {
            return <img src={yellow} alt="" width={20}/>
        } else {
            return <img src={red} alt="" width={20}/>
        }
    } else if (name === "subst") {
        return <RiArrowUpDownFill size={15}/>
    }
}

function OneMatch(props) {
    let id = useParams("id").id;
    const [statis, setStatis] = useState();
    const [awayGrid, setAwayGrid] = useState([]);
    const [away, setAway] = useState();
    const [home, setHome] = useState();
    const [homeGrid, setHomeGrid] = useState([]);
    const [statisHidden, setStatisHidden] = useState(false);
    const [statisList, setStatisList] = useState([]);
    const [statisList2, setStatisList2] = useState([]);
    const main = useSelector(state => state.matches);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);


    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getMatchesStatistic(id));
        } else {
            setStatis(main.matchStatistics);
            setAway(main.matchStatistics?.awayTeam);
            setHome(main.matchStatistics?.homeTeam);
            setStatisList(main.matchStatistics?.homeTeam?.statistic?.statList);
            setStatisList2(main.matchStatistics?.awayTeam?.statistic?.statList);
            gridListFrom(main.matchStatistics?.homeTeam?.statistic?.formation, main.matchStatistics?.awayTeam?.statistic?.formation);
        }
    }, [main.matchStatistics]);

    const fromRed = (num, index) => {
        let number = num + statisList2[index]?.value;
        number = ((num / number) * 100).toFixed(0);
        return 100 - number;
    }
    const fromBlue = (num, index) => {
        let number = num + statisList2[index]?.value;
        number = ((statisList2[index]?.value / number) * 100).toFixed(0);
        return 100 - number;
    }
    const statType = (name) => {
        if (name === "Shots on Goal") {
            return "Aniq zarbalar";
        } else if (name === "Shots off Goal") {
            return "Aniq zarbalar";
        }
        if (name === "Total Shots") {
            return "Umumiy zarbalar";
        } else if (name === "Blocked Shots") {
            return "To'silgan zarbalar";
        } else if (name === "Shots insidebox") {
            return "Darvozaga yo'llangan zarbalar";
        } else if (name === "Shots outsidebox") {
            return "Darvoza chetiga zarbalar";
        } else if (name === "Fouls") {
            return "Jarimalar";
        } else if (name === "Corner Kicks") {
            return "Burchak zarbalari";
        } else if (name === "Offsides") {
            return "O'yindan tashqari holat";
        } else if (name === "Ball Possession") {
            return "To'pga egalik";
        } else if (name === "Yellow Cards") {
            return "Sariq kartochka";
        } else if (name === "Red Cards") {
            return "Qizil kartochka";
        } else if (name === "Goalkeeper Saves") {
            return "Darvozabon save";
        } else if (name === "Total passes") {
            return "Uzatmalar";
        } else if (name === "Passes accurate") {
            return "Aniq uzatmalar";
        } else if (name === "Passes %") {
            return "Uzatmalar % (Foizda)";
        } else {
            return name;
        }
    }

    const gridListFrom = (home, away) => {
        let list = [home, away];
        for (let index = 0; index < list.length; index++) {
            let split = list[index]?.split("-");
            let awayOrHome = [];
            console.log("split", split)
            if (split) {

                for (let splitElement of split) {
                    let helper = [];
                    for (let i = 0; i < splitElement; i++) {
                        helper.push(i + 1);
                    }
                    if (index === 1) {
                        // helper.reverse();
                        awayOrHome.push(helper);
                    } else {
                        awayOrHome.push(helper);
                    }
                }
            }
            if (index === 0) {
                setHomeGrid(awayOrHome);
            } else {
                awayOrHome.reverse();
                setAwayGrid(awayOrHome);
            }
        }
    }

    const homePlayers = (num1, num2) => {
        // eslint-disable-next-line no-unused-expressions
        return home?.statistic?.mainContent?.filter((item => item.grid === `${num1}:${num2}`))[0];
    }

    const awayPlayers = (num1, num2) => {
        // eslint-disable-next-line no-unused-expressions
        return away?.statistic?.mainContent?.filter((item => item.grid === `${num1}:${num2}`))[0];
    }

    return (<Container fluid className={`${homeStyle.main} `}>
        <Row className={'justify-content-center'}>
            <Col xs={12} sm={12} md={6} lg={5} xl={4}>
                <div
                    className={`${homeStyle.backgroundAndBorder} d-flex justify-content-around align-items-center`}>
                    <div className={'w-100'}>
                        <div className={'text-center'}
                             style={{fontWeight: 600}}>{statis?.status !== "Boshlandi" ? <>{new Date(statis?.date).toLocaleDateString()} {" "} {new Date(statis?.date).toLocaleTimeString().substring(0, 5)}</> : statis?.status}</div>
                        <div className={'d-flex justify-content-around'}>
                            <div className={'text-center'}>
                                <img src={statis?.homeTeam?.logo} width={50} className={'mt-4'}/>
                                <br/>
                                <div style={{
                                    fontSize: 15, width: 50
                                }}>

                                </div>
                            </div>
                            {statis?.status === "Boshlanmadi" ? <span
                                    style={{width: 20, backgroundColor: '#0D308C', height: 7, marginTop: 40}}></span> :
                                <span style={{
                                    fontSize: 60, color: '#0D308C', fontWeight: 700
                                }}>{statis?.homeTeam?.goals}</span>}
                            <div className={`${homeStyle.dualDots} mt-4`}>
                                <div></div>
                                <div></div>
                            </div>
                            {statis?.status === "Boshlanmadi" ? <span
                                    style={{width: 20, backgroundColor: '#0D308C', height: 7, marginTop: 40}}></span> :
                                <span style={{
                                    fontSize: 60, color: '#0D308C', fontWeight: 700
                                }}>{statis?.awayTeam?.goals}</span>}
                            <div className={'text-center'}>
                                <img src={statis?.awayTeam?.logo} width={50} className={'mt-4'}/>
                                <br/>
                                <div style={{
                                    fontSize: 15, width: 50
                                }}>
                                </div>
                            </div>
                        </div>
                        <div className={'w-100 d-flex justify-content-between px-2'}><span
                            className={'text-start'}>{statis?.homeTeam.name?.substring(0, 15)}</span> <span
                            className={'text-end'}>{statis?.awayTeam.name?.substring(0, 15)}</span></div>
                        <div className={'text-center'} style={{fontSize: 10, color: '#a8a7a7'}}>
                            <span
                                style={{fontWeight: 500}}>Hakam: </span> {statis?.referee}</div>
                        <div className={'text-center'} style={{fontSize: 10, color: '#a8a7a7'}}><span
                            style={{fontWeight: 500}}>Stadion: </span> {statis?.stadium}</div>
                    </div>
                </div>
            </Col>
        </Row>
        <Row className={'justify-content-center'}>
            <Col xs={12} sm={12} md={6} lg={5} xl={4}>
                <Tabs
                    defaultActiveKey="home"
                    id="uncontrolled-tab-example"
                    className={`${homeStyle.myTabs} mb-1 d-flex justify-content-center fs-6`}>
                    <Tab eventKey="home" title="O'yin jarayoni">
                        <Row>
                            <Col>
                                <div className={homeStyle.backgroundAndBorder}>

                                    {statis?.eventList?.map((item, index) => {
                                        if (item.club === 0) {
                                            return (<div key={index} className={'w-100 d-flex justify-content-between'}>
                                                <div className={homeStyle.events}>
                                                    <div>
                                                    <span style={{
                                                        fontWeight: 600, width: 50
                                                    }}>{item?.playerName.substring(0, 10)}</span>
                                                        <br/>
                                                        {item?.assistName !== null ? <span style={{
                                                            marginLeft: 30, width: 50
                                                        }}>{item?.assistName.substring(0, 10)}</span> : null}
                                                    </div>
                                                    <div>{item?.elapsed} min</div>
                                                    <div>{checkStatusMatch(item.type, item.detail)}</div>
                                                </div>
                                                <div></div>
                                            </div>)
                                        } else {
                                            return (<div key={index} className={'w-100 d-flex justify-content-between'}>
                                                <div></div>
                                                <div className={homeStyle.events}>
                                                    <div>
                                                    <span style={{
                                                        fontWeight: 600, width: 50
                                                    }}>{item?.playerName.substring(0, 10)}</span>
                                                        <br/>
                                                        {item?.assistName !== null ? <span style={{
                                                            marginLeft: 30, width: 50
                                                        }}>{item?.assistName.substring(0, 7)}</span> : null}
                                                    </div>
                                                    <div>{item?.elapsed} min</div>
                                                    <div>{checkStatusMatch(item.type, item.detail)}</div>
                                                </div>
                                            </div>)
                                        }
                                    })}

                                </div>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="profile" title="Statistica">
                        <Row>
                            <div className={homeStyle.backgroundAndBorder}>
                                <div>
                                    {statisList?.map((item, index) => <div key={index}>
                                        <div className={'text-center fs-6'}>{statType(item.type)}</div>
                                        <div className={homeStyle.teamStatis}>
                                            <div style={{width: 50, textAlign: 'center'}}>{item.value}</div>
                                            <div className={'w-100'}>
                                                <div className={'d-flex w-100 justify-content-center px-3'}>
                                                    <div className={homeStyle.blueLine}
                                                         style={{width: `${fromBlue(item.value, index)}%`}}></div>
                                                    <div className={homeStyle.redLine}
                                                         style={{width: `${fromRed(item.value, index)}%`}}></div>
                                                </div>
                                            </div>
                                            <div style={{
                                                width: 50, textAlign: 'center'
                                            }}>{statisList2[index].value}</div>
                                        </div>
                                    </div>)}
                                </div>
                            </div>
                        </Row>
                    </Tab>
                    <Tab eventKey="contact" title="Tarkiblar">
                        <div style={{overflowX: 'auto', width: '100%'}}>
                            <div className={'d-flex'} style={{width: `${200}%`}}>
                                <div className={'w-50'}>
                                    <div className={homeStyle.backgroundAndBorder}>
                                        <div className={'text-center fs-3'}>Asosiy tarkib</div>
                                        <div
                                            className={`w-100 d-flex justify-content-between ${homeStyle.borderBottom}`}>
                                            <div>{
                                                home?.statistic?.mainContent.map((item, index) =>
                                                    <div key={index} className={` p-1 d-flex fs-6`}>
                                                        <span>{index + 1}</span><span
                                                        className={'px-2'}><a href={item?.googleLing} target="_blank"
                                                                              rel="noreferrer noopener"
                                                                              style={{textDecoration: 'none'}}>{item.playerName.substring(0, 10)}</a></span>
                                                    </div>
                                                )
                                            }
                                                <div className={` p-1 d-flex`} style={{fontSize: 15}}><span
                                                    style={{fontWeight: 600}}>Mrb: </span> <span className={'px-2'}
                                                                                                 style={{
                                                                                                     fontWeight: 600,
                                                                                                     color: '#049d0c'
                                                                                                 }}>{home?.statistic?.coachName?.substring(0, 10)}</span>
                                                </div>
                                            </div>
                                            <div>{
                                                away?.statistic?.mainContent.map((item, index) =>
                                                    <div key={index} className={` p-1 d-flex justify-content-end`}><span
                                                        className={'px-2'}><a href={item?.googleLing} target="_blank"
                                                                              rel="noreferrer noopener"
                                                                              style={{textDecoration: 'none'}}>{item.playerName.substring(0, 10)}</a></span><span>{index + 1}</span>
                                                    </div>
                                                )
                                            }
                                                <div className={` p-1 d-flex justify-content-end text-end`}
                                                     style={{fontSize: 15}}><span
                                                    style={{fontWeight: 600}}>Mrb: </span> <br/><span className={'px-2'}
                                                                                                      style={{
                                                                                                          fontWeight: 600,
                                                                                                          color: '#049d0c'
                                                                                                      }}>{away?.statistic?.coachName?.substring(0, 10)}</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div className={'text-center fs-3'}>Zahira</div>
                                        <div className={`w-100 d-flex justify-content-between`}>
                                            <div>{
                                                home?.statistic?.backupContent.map((item, index) =>
                                                    <div key={index} className={` p-1 d-flex fs-6`}>
                                                        <span>{index + 1}</span><span
                                                        className={'px-2'}><a href={item?.googleLing} target="_blank"
                                                                              rel="noreferrer noopener"
                                                                              style={{textDecoration: 'none'}}>{item.playerName?.substring(0, 10)}</a></span>
                                                    </div>
                                                )
                                            }

                                            </div>
                                            <div>{
                                                away?.statistic?.backupContent.map((item, index) =>
                                                    <div key={index} className={` p-1 d-flex justify-content-end`}>

                                                        <span
                                                            className={'px-2'}><a href={item?.googleLing}
                                                                                  target="_blank"
                                                                                  rel="noreferrer noopener"
                                                                                  style={{textDecoration: 'none'}}>{item.playerName?.substring(0, 10)}</a></span><span>{index + 1}</span>
                                                    </div>
                                                )
                                            }

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div className={'w-50 d-flex justify-content-around text-center'}>
                                    <div className={`${homeStyle.stadion}`}>
                                        <Container className={'h-100'}>
                                            <Row style={{height: '50%'}}>
                                                <div className={'justify-content-around d-flex w-100 pt-3'}>
                                                    <div style={{color: 'white', fontSize: 10}}>{
                                                        homePlayers(1, 1)?.playerName?.substring(0, 14)
                                                    }
                                                        <br/>
                                                        <img src={homePlayers(1, 1)?.playerPhoto} width={20} alt=""
                                                             style={{borderRadius: 5}}/>
                                                    </div>
                                                </div>
                                                {
                                                    homeGrid.map((item, index) =>
                                                        <div key={index}
                                                             className={'justify-content-around d-flex w-100 text-center'}>{item.map((item2, index2) =>
                                                            <div key={index2} style={{
                                                                color: 'white',
                                                                fontSize: 10,
                                                                textAlign: 'center'
                                                            }}>
                                                                {
                                                                    homePlayers(index + 2, index2 + 1)?.playerName?.substring(0, 14)
                                                                }
                                                                <br/>
                                                                <img
                                                                    src={homePlayers(index + 2, index2 + 1)?.playerPhoto}
                                                                    alt="" width={20} style={{borderRadius: 5}}/>
                                                            </div>
                                                        )}</div>
                                                    )
                                                }
                                            </Row>
                                            <Row style={{height: '50%'}} className={'pt-3'}>

                                                {
                                                    awayGrid.map((item, index) =>
                                                        <div key={index}
                                                             className={'justify-content-around d-flex w-100'}>{item.map((item2, index2) =>
                                                            <div key={index2} style={{color: 'white', fontSize: 10}}>
                                                                <img src={
                                                                    awayPlayers((awayGrid.length + 1) - index, item2)?.playerPhoto}
                                                                     alt="" width={20} style={{borderRadius: 5}}/>
                                                                <br/>
                                                                {
                                                                    awayPlayers((awayGrid.length + 1) - index, item2)?.playerName?.substring(0, 10)
                                                                }
                                                            </div>
                                                        )}</div>
                                                    )
                                                }
                                                <div className={'justify-content-around d-flex w-100'}>
                                                    <div style={{color: 'white', fontSize: 10}}>
                                                        <img src={awayPlayers(1, 1)?.playerPhoto} width={20} alt=""
                                                             style={{borderRadius: 5}}/>
                                                        <br/>
                                                        <span>{awayPlayers(1, 1)?.playerName?.substring(0, 10)}</span>
                                                    </div>
                                                </div>
                                            </Row>
                                        </Container>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Tab>
                </Tabs>
            </Col>
        </Row>
        <Row className={'justify-content-center'}>
            <Col xs={12} sm={12} md={6} lg={5} xl={4}>
                <div className={`${homeStyle.backgroundAndBorder}`}>
                    <div className={`${homeStyle.borderBottomBold} p-3`}>
                        <div className={`d-flex justify-content-between align-items-center`}>
                            <div className={'text-center'}>
                                <img src={home?.logo} width={30}/>
                            </div>
                            <span>
                           <span className={'fw-bolder fs-5'}>O'zaro o'yinlar</span>
                            <div className={'d-flex justify-content-center'}>
                            <span className={'fw-bolder fs-6'}>{statis?.h2HStatistic.winHome}</span>
                            <span className={'fw-bolder fs-6'}>:</span>
                            <span className={'fw-bolder fs-6'}>{statis?.h2HStatistic?.winAway}</span>
                        </div>
                        </span>
                            <div className={'text-center'}>
                                <img src={away?.logo} width={30}/>
                            </div>
                        </div>
                        <div className={'d-flex justify-content-between'}>
                            <span>
                            <div className={'fw-bolder text-start'}>{home?.name}</div>
                            </span>
                            <span>
                            <div className={'fw-bolder text-end'}>{away?.name}</div>
                            </span>
                        </div>
                    </div>
                    <div>
                        {statis?.h2HStatistic?.matchList.map((item, index) =>
                            <div key={index} className={`${homeStyle.borderBottom} d-flex justify-content-between px-2`}
                                 style={{fontSize: 10}}>
                                <div className={'d-flex justify-content-start'}>
                                    <div>
                                        <img src={item.homeLogo} alt="" width={20}/>
                                    </div>
                                    <span className={'mx-1'}>{item?.homeTeamName?.substring(0, 12)}</span>
                                </div>
                                <div className={'text-center justify-content-center'}>
                                    <div className={'d-flex'}>
                                        <span className={'fw-bolder'}>{item?.leagueName?.substring(0, 12)}</span>
                                        <span className={'mx-2'}>{new Date(item?.date).toLocaleDateString()}</span>
                                    </div>
                                    <div className={'d-flex fw-bolder text-center justify-content-center'}>
                                        <span>{item?.homeTeamGoals}</span>
                                        <span>:</span>
                                        <span>{item?.awayTeamGoals}</span>
                                    </div>
                                </div>
                                <div className={'d-flex justify-content-end'}>

                                    <span className={'mx-1'}>{item?.awayTeamName?.substring(0, 12)}</span>
                                    <div>
                                        <img src={item.awayLogo} alt="" width={20}/>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>

            </Col>
        </Row>

    </Container>);
}

export default OneMatch;