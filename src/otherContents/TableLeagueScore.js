import React from 'react';
import {Table} from 'react-bootstrap';

function TablesLeague(props) {
    const substr = (name,num) => {
        return name.substring(0,num);
    }
    return (
        <>
            {/*<div style={{fontSize:20,textAlign:'center',width:'100%',fontWeight:600}}> {props?.leagueName?.name} Turnir jadvali</div>*/}
            <Table size={'sm'}>
                        <thead>
                        <tr>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>
                               <span style={{width:100}}> {props.leagueName?.name} </span>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>to'purarlar</th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                        </tr>
                        <tr>
                            <th>№</th>
                            <th>Ismi</th>
                            <th>Jamoasi</th>
                            <th>Go</th>
                            <th>As</th>
                            <th>O'y</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.list?.map((team, index) => <tr key={index}>
                            <td>{index + 1}</td>
                            <td><span><img src={team.playerPhoto} width={20} alt=""/></span> <span>{substr(team?.playerName,9)}</span></td>
                            <td className={'d-flex'}><span><img src={team.team_logo} width={20} alt=""/></span> <span>{substr(team?.team_name,7)}</span></td>
                            <td>{team?.total_goals}</td>
                            <td>{team?.total_assists}</td>
                            <td>{team?.appearences}</td>
                        </tr>)}
                        </tbody>
            </Table>
        </>
    );
}

export default TablesLeague;