import React from 'react';
import {Table} from 'react-bootstrap';
import yellow from '../image/yellowCard.png'
import red from '../image/redCard.png'

function TablesLeague(props) {
    const substr = (name,num) => {
        return name.substring(0,num);
    }
    return (
        <>
            {/*<div style={{fontSize:20,textAlign:'center',width:'100%',fontWeight:600}}> {props?.leagueName?.name} Turnir jadvali</div>*/}
            <Table size={'sm'}>
                        <thead>
                        <tr>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}} colSpan={2}>
                               <span style={{width:100}}> {props.leagueName?.name} </span>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}><img src={yellow} alt="" width={30}/></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                        </tr>
                        <tr>
                            <th>№</th>
                            <th>Futbolchi ismi</th>
                            <th>Jamoasi</th>
                            <th><img src={yellow} alt="" width={20}/></th>
                            <th><img src={red} alt="" width={25}/></th>
                            <th>O'y</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.list?.map((team, index) => <tr key={index}>
                            <td>{index + 1}</td>
                            <td style={{width:120}}><span><img src={team.playerPhoto} width={20} alt=""/></span> <span>{substr(team?.playerName,9)}</span></td>
                            <td style={{width:80,display:'flex'}}><span><img src={team.team_logo} width={20} alt=""/></span> <span>{substr(team?.team_name,7)}</span></td>
                            <td>{team?.yellow}</td>
                            <td>{team?.red}</td>
                            <td>{team?.appearences}</td>
                        </tr>)}
                        </tbody>
            </Table>
        </>
    );
}

export default TablesLeague;