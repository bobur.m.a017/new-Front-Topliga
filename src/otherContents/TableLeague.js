import React from 'react';
import {Table} from 'react-bootstrap';
import {Link} from "react-router-dom";

function TablesLeague(props) {
    return (
        <>
            <div style={{fontSize:20,textAlign:'center',width:'100%',fontWeight:600}}> {props?.leagueName?.name} Turnir jadvali</div>
            <Table size={'sm'}>
                {props.list?.map((league, key) =>
                    <>
                        <thead>
                        <tr>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>

                                {league?.name}
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                        </tr>
                        <tr>
                            <th>№</th>
                            <th>Jamoalar nomi</th>
                            <th>O'</th>
                            <th>G/N</th>
                            <th>O</th>
                        </tr>
                        </thead>
                        <tbody>
                        {league?.tournamentTables?.map((team, index) => <tr key={index}>
                            <td>{index + 1}</td>
                            <td><span><img src={team.logo} width={20} alt=""/></span> <span><Link to={'/team/'+team?.teamId} style={{textDecoration:'none'}}>{team.name}</Link></span></td>
                            <td>{team?.allResult?.played}</td>
                            <td>{team?.goalsDiff}</td>
                            <td>{team?.points}</td>
                        </tr>)}
                        </tbody>
                    </>)}
            </Table>
        </>
    );
}

export default TablesLeague;