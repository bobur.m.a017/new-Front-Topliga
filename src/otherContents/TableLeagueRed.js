import React from 'react';
import {Table} from 'react-bootstrap';
import yellow from '../image/yellowCard.png'
import red from '../image/redCard.png'

function TablesLeague(props) {
    const substr = (name,num) => {
        return name.substring(0,num);
    }
    return (
        <>
            {/*<div style={{fontSize:20,textAlign:'center',width:'100%',fontWeight:600}}> {props?.leagueName?.name} Turnir jadvali</div>*/}
            <Table size={'sm'} className={'w-100'}>
                        <thead>
                        <tr>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}} colSpan={2}>
                               <span  className={'d-flex'}>{props.leagueName?.name} </span>
                            </th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}} colSpan={2}><img src={red} alt="" width={40}/></th>
                            <th style={{backgroundColor: 'rgba(196,196,196,0.69)'}}></th>
                        </tr>
                        <tr>
                            <th>№</th>
                            <th>Futbolchi ismi</th>
                            <th>Jamoasi</th>
                            <th><img src={red} alt="" width={25}/></th>
                            <th><img src={yellow} alt="" width={20}/></th>
                            <th>O'y</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.list?.map((team, index) => <tr key={index}>
                            <td>{index + 1}</td>
                            <td ><span className={'d-flex'}> <img src={team.playerPhoto} width={20} height={20} alt=""/><span className={'d-flex'} style={{width:80}}>{substr(team?.playerName,9)}</span></span></td>
                            <td style={{display:'flex'}}><img src={team.team_logo} width={20} alt=""/> <span  className={'d-flex'}>{substr(team?.team_name,7)}</span></td>
                            <td>{team?.red}</td>
                            <td>{team?.yellow}</td>
                            <td>{team?.appearences}</td>
                        </tr>)}
                        </tbody>
            </Table>
        </>
    );
}

export default TablesLeague;