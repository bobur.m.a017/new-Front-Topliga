import { configureStore } from "@reduxjs/toolkit";
import matches from "../src/matches/MatchesReducer";
import main from "../src/main/MainReducer";
import team from "../src/team/TeamReducer";
import news from "../src/news/NewsReducer";
import media from "../src/media/MediaReducer";
import Api from "./Api"

export default configureStore({
    reducer: {
        matches,
        main,
        team,
        news,
        media,
    },
    middleware: [Api],
});