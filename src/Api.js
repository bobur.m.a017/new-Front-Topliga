import axios from "axios";
import {baseUrl} from "./functions/Func";

const api = ({ dispatch }) => (next) => (action) => {
    if (action.type !== 'api/call') {
        next(action);
        return;
    } else {
        const { url, method, data, headers, params, success, error } = action.payload;
        console.log(data, url, "data");
        axios({
            // baseURL: "http://localhost:1122/api/soccer",
            // baseURL: "https://25af-213-230-88-200.eu.ngrok.io/api/soccer",
            baseURL: baseUrl(),
            url,
            method,
            data,
            headers,
            params
        }).then(res => {
            dispatch({
                type: success,
                payload: res.data
            });
            console.log(res.data, "success");
        }).catch(err => {
            dispatch({
                type: error,
                payload: err?.response?.data
            });
            console.log(err?.response?.data, "error");
        });
    }
}
export default api;