import React, {useState} from 'react';
import {useParams} from "react-router-dom";
import homeStyle from '../matches/home.module.scss'
import mediaStyle from './media.module.scss'
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useRef} from "react";
import {getMedia} from "./MediaReducer";
import {IoIosArrowDown, IoIosArrowUp} from "react-icons/io";
import {Button, Col, Container, Row} from "react-bootstrap";
import {useMediaQuery} from "react-responsive";

function Media(props) {
    const g576 = useMediaQuery({query: '(max-width: 576px)'});
    const g768 = useMediaQuery({query: '(max-width: 768px)'});
    const g912 = useMediaQuery({query: '(max-width: 912px)'});
    const [mediaPage, setMediaPage] = useState(0);
    const [medias, setMedias] = useState([]);
    const mediii = [
        1, 2, 3, 4, 5, 6
    ]

    const style1 = {
        width: '100%', height: 0, position: 'relative', paddingBottom: '56.250%', background: '#000'
    }
    const style2 = {
        width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, overflow: 'hidden'
    }
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const media = useSelector(state => state.media);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getMedia());
        } else {
            let list = [...medias];
            setMedias(list.concat(media?.mediaes));
        }
    }, [media?.mediaes]);

    const getMediaWithPage = (number) => {
        dispatch(getMedia({page:number+1}));
        setMediaPage(number+1);
    }
    return (
        <Container fluid={'sm'} className={` ${homeStyle.main} ${mediaStyle.main}`}>
            {medias.map((media, index) =>
                <Row className={`justify-content-center mt-2`} key={index}>
                    <Col xs={12} sm={12} md={7} lg={6} xl={6}>
                        <div>
                            <div className={mediaStyle.backgroundAndBorder}>
                                <div className={'w-100 h-auto'}>
                                    <iframe width="100%" height={g576 ? '200' : g768 ? '250':g912?'250':'300'}
                                            src={media?.url}
                                            frameBorder="0" allowFullScreen></iframe>
                                </div>
                                <div className={'text-center'}>
                                    <span>{media?.title}</span>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            )
            }
            <div className={'d-flex justify-content-center'}>
                <div>
                    <Button variant={'outline-light'} size={'sm'} style={{width:100,margin:10}} onClick={()=>getMediaWithPage(mediaPage)} >Yana</Button>
                </div>
            </div>
        </Container>
    )
        ;
}

export default Media;