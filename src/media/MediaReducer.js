import { createSlice } from "@reduxjs/toolkit";
import { setApi } from "../ApiCall";
const slice = createSlice({
    name: "media",
    initialState: {
        result: {},
        media: [],
        mediaes: [],
        error: {},

    },
    reducers: {
        result: (state, action) => {
            state.result = action.payload;
        },
        media: (state, action) => {
            state.media = action.payload;
        },
        mediaes: (state, action) => {
            state.mediaes = action.payload;
        },
        error: (state, action) => {
            state.error = action.payload;
        },

    }
})

function getToken() {
    return localStorage.getItem("Authorization");
}
function getRole() {
    return localStorage.getItem("role");
}

function getId() {
    return localStorage.getItem("id");
}

export const getMedia = (params) => setApi({
    url: "/news/getVideo",
    method: "get",
    params,
    success: slice.actions.mediaes,
    error: slice.actions.error
});

export const getOneNews = (id) => setApi({
    url: "/media/" + id,
    method: "get",
    success: slice.actions.media,
    error: slice.actions.error
});

export default slice.reducer;