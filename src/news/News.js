import React, {useState} from 'react';
import {useParams} from "react-router-dom";
import homeStyle from '../matches/home.module.scss'
import newsStyle from './news.module.scss'
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useRef} from "react";
import {getNews} from "./NewsReducer";
import {IoIosArrowDown, IoIosArrowUp} from "react-icons/io";
import {Button, Col, Container, Row} from "react-bootstrap";
import {BsEye} from "react-icons/bs";
import {baseUrl} from "../functions/Func";

function News(props) {
    const [newsId, setNewsId] = useState(null);
    const [newsPage, setNewsPage] = useState(0);
    const [newsState,setNewsState] = useState([]);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const news = useSelector(state => state.news);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getNews());
        } else {
             let list = [...newsState];
            setNewsState(list.concat(news?.newses));
        }
    }, [news?.newses]);

    const moreNewsEnable = (id) => {
        if (newsId === id) {
            setNewsId(null);
        } else {
            setNewsId(id);
        }
    }
    const getNewsWithPage = (number) => {
        dispatch(getNews({page:number+1}));
        setNewsPage(number+1);
    }
    return (
        <Container fluid={'sm'} className={` ${homeStyle.main} ${newsStyle.main}`}>
            {newsState?.map((news, index) =>
                <Row className={'justify-content-center mt-2'} key={index}>
                    <Col xs={12} sm={12} md={7} lg={6} xl={6}>
                        <div>
                            <div className={newsStyle.backgroundAndBorder}>
                                <div className={`d-flex justify-content-around`}
                                     onClick={() => moreNewsEnable(news.id)} style={{cursor: 'pointer'}}>
                                    <div className={newsStyle.imgs} style={{backgroundImage: `url(${baseUrl()}/news/getFile/${news.photoId})`}}>
                                    </div>
                                    <div className={'w-100  position-relative'} style={{paddingLeft: 10}}>
                                        <div className={'w-100 d-flex justify-content-end align-items-center'}
                                             style={{fontSize: 10, lineHeight: '10%'}}>
                                            <span className={'mx-2'}>{new Date(news.downloadedTime).toLocaleDateString()}</span>
                                            {/*<span className={'mx-2'}>{news.shareLink}</span>*/}
                                            <span className={'mx-1 d-flex'}>{news.watchCounter}</span>
                                            <BsEye size={15}/>
                                        </div>
                                        <div>
                                            <span className={newsStyle.newsTitle}>{news.paragraph} {news?.id === newsId ?
                                               <span className={newsStyle.arrows}> <IoIosArrowUp size={30}/></span> :
                                                <span className={newsStyle.arrows}> <IoIosArrowDown size={30}/></span>}</span>

                                            {/*<span className={newsStyle.newsText}>{news.text.substring(0, 200)} ...</span>*/}
                                            {/*<div className={'justify-content-end d-flex'}>*/}
                                            {/*    {news?.id === newsId ? <IoIosArrowUp size={30}/> :*/}
                                            {/*        <IoIosArrowDown size={30}/>}*/}
                                            {/*</div>*/}
                                        </div>

                                    </div>

                                </div>
                                <div className={'w-100 p-3'}
                                     style={news?.id === newsId ? {backgroundColor: 'rgba(243,243,243,0.7)'} : {display: 'none'}}>
                                    <span>
                                        {news.text}
                                    </span>
                                    <br/>
                                    <span className={'fw-bolder'}>Manba: {news.newsAuthor}</span>
                                </div>
                            </div>
                        </div>

                    </Col>
                </Row>
            )
            }
            <div className={'d-flex justify-content-center'}>
                    <div>
                        <Button variant={'outline-light'} size={'sm'} style={{width:100,margin:10}} onClick={()=>getNewsWithPage(newsPage)} >Yana</Button>
                    </div>
            </div>
        </Container>
    )
        ;
}

export default News;