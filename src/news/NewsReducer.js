import { createSlice } from "@reduxjs/toolkit";
import { setApi } from "../ApiCall";
const slice = createSlice({
    name: "news",
    initialState: {
        result: {},
        news: [],
        newses: [],
        error: {},

    },
    reducers: {
        result: (state, action) => {
            state.result = action.payload;
        },
        news: (state, action) => {
            state.news = action.payload;
        },
        newses: (state, action) => {
            state.newses = action.payload;
        },
        error: (state, action) => {
            state.error = action.payload;
        },

    }
})

function getToken() {
    return localStorage.getItem("Authorization");
}
function getRole() {
    return localStorage.getItem("role");
}

function getId() {
    return localStorage.getItem("id");
}

export const getNews = (params) => setApi({
    url: "/news",
    method: "get",
    params,
    success: slice.actions.newses,
    error: slice.actions.error
});

export const getOneNews = (id) => setApi({
    url: "/news/" + id,
    method: "get",
    success: slice.actions.news,
    error: slice.actions.error
});

export default slice.reducer;