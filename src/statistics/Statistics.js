import React from 'react';
import homeStyle from "../matches/home.module.scss";
import arrowBack from "../image/arrowBack.png";
import {Col, Container, Form, InputGroup, Row} from "react-bootstrap";
import {IoMdArrowRoundBack, IoMdArrowRoundForward} from "react-icons/io";
import {Link} from "react-router-dom";
import TableLeague from "../otherContents/TableLeague";
import TableLeagueScore from "../otherContents/TableLeagueScore";
import TableLeagueAssists from "../otherContents/TableLeagueAssists";
import TableLeagueYellow from "../otherContents/TableLeagueYellow";
import TableLeagueRed from "../otherContents/TableLeagueRed";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useRef, useState} from "react";
import mainLogo from "../image/uefa.svg";
import fifa from "../image/fifa.svg";
import top5 from "../image/top5.png";
import otherLeague from "../image/otherLeague.png";
import {getCountry, getLeagues, getMainMatches, getMainMatchesBy, getTop} from "../main/MainReducer";
import {BiImage} from "react-icons/bi";
import {BsSearch} from "react-icons/bs";

function Statistics(props) {
    const main = useSelector(state => state.main);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const [leagues, setLeagues] = useState([]);
    const [leaguesTop, setLeaguesTop] = useState([]);
    const [leaguesTopSub, setLeaguesTopSub] = useState([]);
    const logoList = [
        {
            id: 1,
            name: "UEFA",
            logo: mainLogo
        },
        {
            id: 2,
            name: "FIFA",
            logo: fifa
        },
        {
            id: 3,
            name: "LIGA",
            logo: top5
        },
        {
            id: 4,
            name: "CUB",
            logo: otherLeague
        },

    ];

    useEffect(() => {
        console.log(main.mainMatches, ",main.mainMatches");
        console.log(main.leaguesTop, ",leaguesTop");
        console.log(main.leagues, ",leagues");
        setLeagues(main.mainMatches);
    }, [main.leagues, main.mainMatches])

    useEffect(() => {
        setLeaguesTop(main.leaguesTop);
        setLeaguesTopSub(main.leaguesTop);
    }, [main.leaguesTop]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            // dispatch(getLeagues());
            dispatch(getCountry());
            dispatch(getMainMatches({leagueId: 132}));
        } else {
            // mainLeagues();
        }
    }, []);

    const getMainData = (data) => {
        if (data === null) {
            setLeaguesTop(main.leaguesTop);
            setLeaguesTopSub(main.leaguesTop);
        } else if (data.leagueId) {
            dispatch(getMainMatchesBy({leagueId: data.id}));
        } else {
            let leagues = main.leaguesTop.filter(item => item.name === data.name);
            if (leagues[0]?.leagues?.length > 0) {
                setLeaguesTop(leagues[0]?.leagues);
                setLeaguesTopSub(leagues[0]?.leagues);
            }
        }
    }

    const getMatchesByRoundAndSeason = (seasonYear, round, id) => {
        dispatch(getMainMatchesBy({leagueId: id, seasonYear, round}));
    }

    const search = (e) => {
        if (e.target.value === '') {
            setLeaguesTop(leaguesTopSub);
        } else {
            setLeaguesTop(leaguesTopSub?.filter(item => item?.nameUz?.toLowerCase().includes(e.target.value.toLowerCase())));
        }
    }

    return (
        <div className={homeStyle.main}>
            <InputGroup className="mb-1">
                <InputGroup.Text id="basic-addon1"><BsSearch/></InputGroup.Text>
                <Form.Control
                    placeholder="Qidirish"
                    name={'search'}
                    onChange={search}
                />
            </InputGroup>
            <div style={{textAlign: 'center', overflowX: 'auto'}}>
                <div style={{display: 'inline-flex'}}
                     className='myOver'>
                    {leaguesTop.length !== 0 && !leaguesTop[0]?.leagues ?
                        <div className={'logoRender'} onClick={() => getMainData(null)}>
                            <img
                                src={arrowBack}
                                height={80}
                                width={80} alt="logo"
                                className="leagues2" style={{backgroundColor: 'white'}}/>
                            <div style={{
                                backgroundColor: 'rgba(217,217,217,0.83)',
                                padding: 3,
                                borderBottomLeftRadius: 8,
                                borderBottomRightRadius: 8
                            }}>Ortga
                            </div>
                        </div> : null}
                    {
                        leaguesTop.map((league, index) =>
                            <div key={index} className={'logoRender2'}
                                 onClick={() => getMainData(league)}>
                                {league.flag !== null ? <img src={league.flag ? league.flag : league.logo} height={80}
                                                             alt="logo"
                                                             className="leagues2"/> :
                                    <BiImage size={80} style={{backgroundColor: '#fff'}} className={'leagues2'}/>}
                                <div style={{
                                    backgroundColor: 'rgba(217,217,217,0.83)',
                                    padding: 3,
                                    borderBottomLeftRadius: 8,
                                    borderBottomRightRadius: 8
                                }}>{league?.nameUz?.substring(0, 8)}</div>
                            </div>
                        )
                    }
                </div>
            </div>
            <Container fluid>
                <Row className={'justify-content-center'}>
                    <Col xs={12} sm={12} md={5} lg={4} xl={3} className={'mt-1'}>
                        <div className={homeStyle.mainMatch} style={leagues?.league ? {} : {height: '67vh'}}>

                            <div className={`${homeStyle.mainMatchNav} shadow`} style={{top: 0}}>
                                <div className={'w-100'}>
                                    <div className={`d-flex justify-content-between align-items-center`}>
                                        <div className={homeStyle.fromIcons}
                                             onClick={() => getMatchesByRoundAndSeason(leagues.season - 1, leagues?.round.number, leagues?.league.id)}>
                                            <IoMdArrowRoundBack/></div>
                                        <div className={'w-75 text-center'}>
                                            <span className={'text-center'}>
                                                Mavsum: <span
                                                className={homeStyle.matchContentText}>{leagues?.season?.yearName}</span>
                                            </span>
                                        </div>
                                        <div className={homeStyle.fromIcons}
                                             onClick={() => getMatchesByRoundAndSeason(leagues.season.year + 1, leagues?.round.number, leagues?.league.id)}>
                                            <IoMdArrowRoundForward/></div>
                                    </div>
                                </div>
                            </div>
                            <div className={`${homeStyle.mainMatchNav} shadow`} style={{top: 40}}>
                                <div className={'w-100'}>
                                    <div className={`d-flex justify-content-between align-items-center`}>
                                        <div className={homeStyle.fromIcons}
                                             onClick={() => getMatchesByRoundAndSeason(leagues.season.year, leagues?.round.number - 1, leagues?.league.id)}>
                                            <IoMdArrowRoundBack size={30}/></div>
                                        <div className={'w-75 text-center'}>
                                            <span className={'text-center'}>
                                                Tur: <span
                                                className={homeStyle.matchContentText}>{leagues?.matches && leagues?.matches.length !== 0 ? leagues?.matches[0].round : null}</span>
                                            </span>
                                        </div>
                                        <div className={homeStyle.fromIcons}
                                             onClick={() => getMatchesByRoundAndSeason(leagues.season.year, leagues?.round.number + 1, leagues?.league.id)}>
                                            <IoMdArrowRoundForward size={30}/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {
                                leagues?.matches?.length !== 0 ? leagues?.matches?.map((match, key) =>
                                    <div key={key} className={homeStyle.mainContentMatch}>
                                        <Link to={"/match/" + match.id}>
                                            <div className={homeStyle.contentMatch}>
                                                <img
                                                    src={match?.homeLogo}
                                                    alt="logo" height={25}/>
                                                <span
                                                    style={{fontWeight: 600}}>{match?.homeTeamGoals === null ? <>-</> : match?.homeTeamGoals}</span>
                                                <div style={{height: '100%', marginTop: -10}}>
                                                    <span style={{fontSize: 10}}>{match.leagueName}</span> <br/>
                                                    <span
                                                        style={{fontWeight: 600}}>{
                                                        <span>{new Date(match?.date).toLocaleDateString()}</span>}{" "}{
                                                        <span
                                                            style={{fontWeight: 700}}>{new Date(match?.date).toLocaleTimeString().substring(0, 5)}</span>}</span>
                                                </div>
                                                <span
                                                    style={{fontWeight: 600}}>{match?.awayTeamGoals === null ? <>-</> : match?.awayTeamGoals}</span>
                                                <img
                                                    src={match?.awayLogo}
                                                    alt="logo" height={25}/>
                                            </div>
                                            <div className={homeStyle.temaName}>
                                                <div>{match?.homeTeamName}</div>
                                                <div>{match?.awayTeamName}</div>
                                            </div>
                                        </Link>
                                    </div>
                                ) : <div className={homeStyle.mainContentMatch}>
                                    <div className={`${homeStyle.contentMatch} justify-content-around`}>
                                        <span className={'text-center'}>Bu kunda (turda) o'yinlar yo'q</span>
                                    </div>
                                    <div className={homeStyle.temaName}>

                                    </div>
                                </div>
                            }

                        </div>
                    </Col>
                    {leagues?.league ? <>
                        <Col xs={12} sm={12} md={7} lg={4} xl={3} className={'fromTable mt-1'}>
                            <TableLeague list={leagues?.table} leagueName={leagues.league}/>
                        </Col>

                        <Col xs={12} sm={12} md={6} lg={4} xl={3} className={'fromTable mt-1'}>
                            <TableLeagueScore list={leagues?.topScores?.goal} leagueName={leagues.league}/>
                        </Col>

                        <Col xs={12} sm={12} md={6} lg={4} xl={3} className={'fromTable mt-1'}>
                            <TableLeagueAssists list={leagues?.topScores?.assist} leagueName={leagues.league}/>
                        </Col>
                        <Col xs={12} sm={6} md={6} lg={4} xl={3} className={'fromTable mt-1'}>
                            <TableLeagueYellow list={leagues?.topScores?.yellow} leagueName={leagues.league}/>
                        </Col>
                        <Col xs={12} sm={6} md={6} lg={4} xl={3} className={'fromTable mt-1'}>
                            <TableLeagueRed list={leagues?.topScores?.red} leagueName={leagues.league}/>
                        </Col>
                    </> : null}
                </Row>
            </Container>
        </div>
    );
}

export default Statistics;