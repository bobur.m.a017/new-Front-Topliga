import {Link, Route, Routes} from 'react-router-dom';
import {ToastContainer} from 'react-toastify';
import {AiOutlineHome, AiOutlineBarChart, AiOutlinePlayCircle, AiOutlineAppstore} from 'react-icons/ai';
import {BsNewspaper} from 'react-icons/bs';
import 'react-toastify/dist/ReactToastify.css';
import Home from './matches/Home';
import {RiNewspaperLine} from "react-icons/ri";
import OneMatch from "./matches/oneMatch";
import Player from "./player/Player";
import Team from "./team/Team";
import Statistics from "./statistics/Statistics";
import News from "./news/News";
import Media from "./media/Media";

function App() {

    return (
        <div>
            <div className='mobile'>
                <nav>
                    <ul>
                        <li>
                            <div>
                                <Link to={'/'} style={{color: 'black', textDecoration: 'none'}}>
                                    <AiOutlineAppstore size={20}/>
                                    <div>Asosiy</div>
                                </Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={'/statistics'} style={{color: 'black', textDecoration: 'none'}}>
                                    <AiOutlineBarChart size={20}/>
                                    <div>Statistika</div>
                                </Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={'/news'} style={{color: 'black', textDecoration: 'none'}}>
                                    <RiNewspaperLine size={20}/>
                                    <div>Yangiliklar</div>
                                </Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={'/media'} style={{color: 'black', textDecoration: 'none'}}>
                                    <AiOutlinePlayCircle size={20}/>
                                    <div>Videolar</div>
                                </Link>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className='desktop'>
                <nav>
                    <ul>
                        <li>
                            <div>
                                <Link to={"/"}>Asosiy</Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={"/statistics"}>Statistika</Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={"/news"}>Yangiliklar</Link>
                            </div>
                        </li>
                        <li>
                            <div>
                                <Link to={"/media"}>Videolar</Link>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
            <div className='marginBottom'>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/match/:id" element={<OneMatch/>}/>
                    <Route path="/player/:id" element={<Player/>}/>
                    <Route path="/team/:id" element={<Team/>}/>
                    <Route path="/news/:id" element={<OneMatch/>}/>
                    <Route path="/news" element={<News/>}/>
                    <Route path="/media" element={<Media/>}/>
                    <Route path="/statistics" element={<Statistics/>}/>
                </Routes>
                <ToastContainer/>
            </div>
        </div>
    );
}

export default App;
