import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import './icons.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import { Provider } from 'react-redux';
import Store from './Store';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
  <Provider store={Store}>
    <BrowserRouter>
        <div className={"appBody"}></div>
      <App />
    </BrowserRouter>
  </Provider>
  // {/* </React.StrictMode> */}
);