import { createSlice } from "@reduxjs/toolkit";
import { setApi } from "../ApiCall";
const slice = createSlice({
    name: "main",
    initialState: {
        result: {},
        error: {},
        leagues: [],
        leaguesTop: [],
        table: [],
        mainMatches: {},
    },
    reducers: {
        mainMatches: (state, action) => {
            state.mainMatches = action.payload;
        },
        leaguesTop: (state, action) => {
            state.leaguesTop = action.payload;
        },
        leagues: (state, action) => {
            state.leagues = action.payload;
        },
        table: (state, action) => {
            state.table = action.payload;
        },
        result: (state, action) => {
            state.result = action.payload;
        },
        error: (state, action) => {
            state.error = action.payload;
        },

    }
})

function getToken() {
    return localStorage.getItem("Authorization");
}
function getRole() {
    return localStorage.getItem("role");
}

function getId() {
    return localStorage.getItem("id");
}



export const getLeagues = () => setApi({
    url: "/league",
    method: "GET",
    success: slice.actions.leagues,
    error: slice.actions.error
});

export const getTop = () => setApi({
    url: "/league/getTop",
    method: "GET",
    success: slice.actions.leaguesTop,
    error: slice.actions.error
});

export const getCountry = () => setApi({
    url: "/country",
    method: "GET",
    success: slice.actions.leaguesTop,
    error: slice.actions.error
});

export const getTable = (params) => setApi({
    url: "/table",
    method: "GET",
    params,
    success: slice.actions.table,
    error: slice.actions.error
});

export const getMainMatches = (params) => setApi({
    url: "/match/byAndLeagueId",
    method: "GET",
    params,
    success: slice.actions.mainMatches,
    error: slice.actions.error
});


export const getMainMatchesBy = (params) => setApi({
    url: "/league/getAllDataLeague",
    method: "GET",
    params,
    success: slice.actions.mainMatches,
    error: slice.actions.error
});

export default slice.reducer;