import React from 'react';
import homeStyle from '../matches/home.module.scss'
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {useRef} from "react";
import {useEffect} from "react";
import {getTeam} from "./TeamReducer";
import {Col, Container, Row, Table} from "react-bootstrap";

function Team(props) {
    let id = useParams("id").id;
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const team = useSelector(state => state.team);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getTeam(id));
        } else {
            console.log(team.team, "team")
        }
    }, [team?.team]);

    return (
        <div className={homeStyle.main}>
            <Container fluid>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12} xl={4}>
                        <div className={`p-5 fw-normal ${homeStyle.backgroundAndBorder}`}>
                            <div className={'text-center fw-bolder'}>{team.team?.name}</div>
                            <div className={`p-2 mt-3 justify-content-between d-flex ${homeStyle.borderBottom}`}>
                                <div className={'fw-bolder'}>Jamoa nomi</div>
                                <div>{team.team?.name}</div>
                            </div>
                            <div className={`p-2 mt-3 justify-content-between d-flex ${homeStyle.borderBottom}`}>
                                <div className={'fw-bolder'}>Logotipi</div>
                                <img src={team.team?.logo} alt="logo" width={50}/>
                            </div>
                            <div className={`p-2 justify-content-between d-flex ${homeStyle.borderBottom}`}>
                                <div className={'fw-bolder'}>Davlati nomi</div>
                                <div>{team.team?.countryName}</div>
                            </div>
                            <div className={`p-2 justify-content-between d-flex ${homeStyle.borderBottom}`}>
                                <div className={'fw-bolder'}>Tashkil qilingan sana</div>
                                <div>{new Date(team.team?.createDate).toLocaleDateString()}</div>
                            </div>
                            <div className={`p-2 mt-3 justify-content-between d-flex ${homeStyle.borderBottom}`}>
                                <div className={'fw-bolder'}>Davlat bayrog'i</div>
                                <img src={team.team?.countryFlag} alt="logo" width={50}/>
                            </div>
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={4}>
                        <div className={`${homeStyle.backgroundAndBorder}`}>
                            <div className={`text-center fw-bolder mb-2 ${homeStyle.borderBottomBold}`}>O'tgan
                                o'yinlari
                            </div>
                            {team.team?.next10match?.map((item, index) =>
                                <div className={`${homeStyle.borderBottom} d-flex justify-content-between px-2`}
                                     style={{fontSize: 10}}>
                                    <div className={'d-flex justify-content-start'}>
                                        <div>
                                            <img src={item.homeLogo} alt="" width={20}/>
                                        </div>
                                        <span className={'mx-1'}>{item?.homeTeamName?.substring(0, 12)}</span>
                                    </div>
                                    <div className={'text-center justify-content-center'}>
                                        <div className={'d-flex'}>
                                            <span className={'fw-bolder'}>{item?.leagueName.substring(0, 10)}</span>
                                            <span className={'mx-2'}>{new Date(item?.date).toLocaleDateString()}</span>
                                        </div>
                                        <div className={'d-flex fw-bolder text-center justify-content-center'}>
                                            <span>{item?.homeTeamGoals}</span>
                                            <span>:</span>
                                            <span>{item?.awayTeamGoals}</span>
                                        </div>
                                    </div>
                                    <div className={'d-flex justify-content-end'}>

                                        <span className={'mx-1'}>{item?.awayTeamName?.substring(0, 12)}</span>
                                        <div>
                                            <img src={item.awayLogo} alt="" width={20}/>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={4}>
                        <div className={`${homeStyle.backgroundAndBorder}`}>
                            <div className={`text-center fw-bolder mb-2 ${homeStyle.borderBottomBold}`}>Bo'lajak
                                o'yinlari
                            </div>
                            {team.team?.last10match?.map((item, index) =>
                                <div className={`${homeStyle.borderBottom} d-flex justify-content-between px-2`}
                                     style={{fontSize: 10}}>
                                    <div className={'d-flex justify-content-start'}>
                                        <div>
                                            <img src={item.homeLogo} alt="" width={20}/>
                                        </div>
                                        <span className={'mx-1'}>{item?.homeTeamName?.substring(0, 12)}</span>
                                    </div>
                                    <div className={'text-center justify-content-center'}>
                                        <div className={'d-flex'}>
                                            <span className={'fw-bolder'}>{item?.leagueName?.substring(0, 12)}</span>
                                            <span className={'mx-2'}>{new Date(item?.date).toLocaleDateString()}</span>
                                        </div>
                                        <div className={'d-flex fw-bolder text-center justify-content-center'}>
                                            <span>{item?.homeTeamGoals}</span>
                                            <span>:</span>
                                            <span>{item?.awayTeamGoals}</span>
                                        </div>
                                    </div>
                                    <div className={'d-flex justify-content-end'}>

                                        <span className={'mx-1'}>{item?.awayTeamName?.substring(0, 12)}</span>
                                        <div>
                                            <img src={item.awayLogo} alt="" width={20}/>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={4}>
                        <div className={`${homeStyle.backgroundAndBorder}`}>
                            <div className={`text-center fw-bolder mb-2 ${homeStyle.borderBottomBold}`}>Jamoa tarkibi
                            </div>
                            <Table size={'sm'}>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Ismi</th>
                                    <th>Yo</th>
                                    <th>T/y</th>
                                    <th>V</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    team.team?.playerList?.map((item, index) =>
                                        <tr key={index} style={{fontSize:10}}>
                                            <td>{index+1}</td>
                                            <td className={'d-flex align-items-center'}><img src={item?.photo} alt="" width={20} style={{borderRadius:10,marginRight:5}}/><a href={item?.googleLink} target="_blank" rel="noreferrer noopener" style={{textDecoration:'none'}}> {item?.name}</a></td>
                                            <td>{item?.age}</td>
                                            <td>{item?.birthDate}</td>
                                            <td>{item?.nationality}</td>
                                        </tr>
                                    )
                                }
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Team;