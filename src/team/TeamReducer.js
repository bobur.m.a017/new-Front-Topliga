import { createSlice } from "@reduxjs/toolkit";
import { setApi } from "../ApiCall";
const slice = createSlice({
    name: "team",
    initialState: {
        result: {},
        team: [],
        error: {},

    },
    reducers: {
        result: (state, action) => {
            state.result = action.payload;
        },
        team: (state, action) => {
            state.team = action.payload;
        },
        error: (state, action) => {
            state.error = action.payload;
        },

    }
})

function getToken() {
    return localStorage.getItem("Authorization");
}
function getRole() {
    return localStorage.getItem("role");
}

function getId() {
    return localStorage.getItem("id");
}

export const getTeam = (id) => setApi({
    url: "/team/" + id,
    method: "get",
    success: slice.actions.team,
    error: slice.actions.error
});

export default slice.reducer;